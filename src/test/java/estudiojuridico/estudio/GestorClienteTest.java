package estudiojuridico.estudio;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class GestorClienteTest {
    GestorCliente gestor;
    
    @Before
    public void before(){
        gestor = new GestorCliente();
    }

    @Test
    public void agregarClienteTest(){
        Domicilio domicilio = new DomicilioSimple("Mitre", 450);
        Cliente cliente = new Cliente("Luis Fernando", "Carpanchay", 41371579L, "Consulta", "carpanchayluis55@gmail.com", 3868457398L, domicilio);
        gestor.agregarCliente(cliente);
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(cliente);
        assertEquals(clientes, gestor.getClientes());
    }

    @Test
    public void modificarClienteTest(){
        Domicilio domicilio = new DomicilioSimple("Mitre", 450);
        Cliente cliente = new Cliente("Luis Fernando", "Carpanchay", 41371579L, "Consulta", "carpanchayluis55@gmail.com", 3868457398L, domicilio);
        gestor.agregarCliente(cliente);
        Cliente clienteModificado = new Cliente("Luis Fernando", "Carpanchay", 45789123L, "Consulta", "carpanchayluis55@gmail.com", 3874597861L, domicilio);
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(clienteModificado);
        gestor.modificarCliente(41371579L, clienteModificado);
        assertEquals(clientes, gestor.getClientes());
    }

    @Test
    public void eliminarClienteTest(){
        Domicilio domicilio = new DomicilioSimple("Mitre", 450);
        Cliente cliente = new Cliente("Luis Fernando", "Carpanchay", 41371579L, "Consulta", "carpanchayluis55@gmail.com", 3868457398L, domicilio);
        Domicilio domicilio2 = new DomicilioEdificio("Prado", 400, "tercer", "13");
        Cliente cliente2 = new Cliente("Evelyn", "Coronel", 45789123L, "Consulta", "evelyn98@gmail.com", 3874597861L, domicilio2);
        gestor.agregarCliente(cliente);
        gestor.agregarCliente(cliente2);
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(cliente2);
        gestor.eliminarCliente(41371579L);
        assertEquals(clientes, gestor.getClientes());
    }

    @Test
    public void ordenarClientesPorDniTest(){
        Domicilio domicilio = new DomicilioSimple("Mitre", 450);
        Cliente cliente = new Cliente("Luis Fernando", "Carpanchay", 41371579L, "Consulta", "carpanchayluis55@gmail.com", 3868457398L, domicilio);
        Domicilio domicilio2 = new DomicilioEdificio("Prado", 400, "tercer", "13");
        Cliente cliente2 = new Cliente("Evelyn", "Coronel", 45789123L, "Consulta", "evelyn98@gmail.com", 3874597861L, domicilio2);
        Cliente cliente3 = new Cliente("Juana", "Reyes", 39784587L, "Consulta", "juanitaS@gmail.com", 3874597861L, domicilio2);
        gestor.agregarCliente(cliente);
        gestor.agregarCliente(cliente2);
        gestor.agregarCliente(cliente3);
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(cliente3);
        clientes.add(cliente);
        clientes.add(cliente2);
        gestor.ordenarClientesPorDNI();
        assertEquals(clientes, gestor.getClientes());
    }

    @Test
    public void ordenarClientesPorApellidoTest(){
        Domicilio domicilio2 = new DomicilioEdificio("Prado", 400, "tercer", "13");
        Cliente cliente1 = new Cliente("Evelyn", "Coronel", 45789123L, "Consulta", "evelyn98@gmail.com", 3874597861L, domicilio2);
        Cliente cliente2 = new Cliente("Juana", "Reyes", 39784587L, "Consulta", "juanitaS@gmail.com", 3874597861L, domicilio2);
        Domicilio domicilio = new DomicilioSimple("Mitre", 450);
        Cliente cliente3 = new Cliente("Luis Fernando", "Carpanchay", 41371579L, "Consulta", "carpanchayluis55@gmail.com", 3868457398L, domicilio);
        gestor.agregarCliente(cliente1);
        gestor.agregarCliente(cliente2);
        gestor.agregarCliente(cliente3);
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(cliente3);
        clientes.add(cliente1);
        clientes.add(cliente2);
        gestor.ordenarClientesPorApellido();
        assertEquals(clientes, gestor.getClientes());
    }

}
