package estudiojuridico.estudio;

public class DomicilioSimple extends Domicilio{
    public DomicilioSimple(String calle, int numeroDeCasa){
        super(calle, numeroDeCasa);
    }
    
    @Override
    public String mostrarDatosDeDomicilio(){
        return "Calle: "+getCalle() + "Numero: "+getnumeroDeCasa();
    }
}