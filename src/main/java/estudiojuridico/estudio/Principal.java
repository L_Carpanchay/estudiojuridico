package estudiojuridico.estudio;

import estudiojuridico.ventana.VentanaPrincipal;
public class Principal{

    public static void main(String[] args){
        GestorCliente clientes = new GestorCliente();
        
        VentanaPrincipal v1 = new VentanaPrincipal(clientes);
        v1.setVisible(true);
        
    }
}