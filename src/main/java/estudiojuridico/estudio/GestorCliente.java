package estudiojuridico.estudio;

import estudiojuridico.excepciones.ClienteExistenteException;
import estudiojuridico.excepciones.ClienteInexistenteException;
import java.util.ArrayList;
import java.util.Collections;

public class GestorCliente {
    //private String nombreCompletoDelResponsableDelEstudio;
    
    private ArrayList<Cliente> clientes;

    public GestorCliente() {
            
        clientes = new ArrayList<Cliente>();
    }

    public Cliente getCliente(Long dni)throws ClienteInexistenteException{
        Cliente clienteEncontrado = null;
        for(Cliente var: clientes){
            if(var.getdni().equals(dni)){
                clienteEncontrado = var;
            }
        }
        if(clienteEncontrado == null){
            throw new ClienteInexistenteException();
        }
        return clienteEncontrado;
    }
    
    public ArrayList<Cliente> getClientes(){
        return clientes;
    }

    //Metodos
    public void modificarCliente(Long dni, Cliente clienteModificado)throws ClienteInexistenteException, ClienteExistenteException{
        Cliente clienteEncontrado;
        clienteEncontrado = getCliente(dni);
        clientes.remove(clienteEncontrado);
        this.agregarCliente(clienteModificado);
    }

    public void eliminarCliente(Long dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado;
        clienteEncontrado = getCliente(dni);
        clientes.remove(clienteEncontrado);
    }
    
    public void agregarCliente(Cliente cliente) throws ClienteExistenteException{
        for(Cliente var: clientes){
            if(cliente.getdni().equals(var.getdni())){
                throw new ClienteExistenteException();
            }
        }
        clientes.add(cliente);
    }

    public void ordenarClientesPorDNI(){
        
        Collections.sort(clientes);
    }
    
    public void ordenarClientesPorApellido(){
        Collections.sort(clientes, new ComparadorDeClientesPorApellido());
    }
}