package estudiojuridico.estudio;

public abstract class Domicilio{
    private String calle;
    private int numeroDeCasa;

    public Domicilio(String calle, Integer numeroDeCasa){
        this.calle = calle;
        this.numeroDeCasa = numeroDeCasa;
    }

    public void setCalle(String calle){
        this.calle = calle;
    }
    public String getCalle(){
        return calle;
    }

    public void setnumeroDeCasa(Integer numeroDeCasa){
        this.numeroDeCasa = numeroDeCasa;
    }
    public int getnumeroDeCasa(){
        return numeroDeCasa;
    }

    public String mostrarDatosDeDomicilio(){
        return "Calle: "+calle + "Numero: "+numeroDeCasa;
    }
}