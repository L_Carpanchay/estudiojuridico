package estudiojuridico.estudio;

import javax.swing.JTextField;

public class Cliente implements Comparable<Cliente>{
    //atributos
    private String nombreCompleto;
    private String apellido;
    private Long dni;
    private String asunto;
    private String correoElectronico;
    private Long numeroTelefonico;
    private Domicilio domicilio;

    //constructor
    public Cliente(String nombreCompleto, String apellido, Long dni, String asunto, String correoElectronico, Long numeroTelefonico, Domicilio domicilio) {
        this.nombreCompleto = nombreCompleto;
        this.apellido = apellido;
        this.dni = dni;
        this.asunto = asunto;
        this.correoElectronico = correoElectronico;
        this.numeroTelefonico = numeroTelefonico;
        this.domicilio = domicilio;
    }


    //getters y setters
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    
    public String getNombreCompleto(){
        return nombreCompleto;
    }

    public void setdni(Long dni){
        this.dni = dni;
    }

    public Long getdni(){
        return dni;
    }

    public void setAsunto(String asunto){
        this.asunto = asunto;
    }
    
    public String getAsunto(){
        return asunto;
    }

    public void setCorreoElectronico(String correoElectronico){
        this.correoElectronico = correoElectronico;
    }
    
    public String getCorreoElectronico(){
        return correoElectronico;
    }

    public void setnumeroTelefonico(Long numeroTelefonico){
        this.numeroTelefonico = numeroTelefonico;
    }
    
    public Long getnumeroTelefonico(){
        return numeroTelefonico;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    //metodos

    @Override
    public int compareTo(Cliente t){
        return dni.compareTo(t.getdni());  
    }

    public String toString() {
        return "Nombre: " + nombreCompleto + ", Apellido: " + apellido + ", DNI:" + dni + ", Asunto: " + asunto + ", correoElectronico: " + correoElectronico + ", Telefono: " + numeroTelefonico + ", Domicilio: " + domicilio.mostrarDatosDeDomicilio();
    }
}