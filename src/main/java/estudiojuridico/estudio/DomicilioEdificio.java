package estudiojuridico.estudio;

public class DomicilioEdificio extends Domicilio{
    private String piso;
    private String departamento;

    public DomicilioEdificio(String calle, int numeroDeCasa, String piso, String departamento){
        super(calle, numeroDeCasa);
        this.departamento = departamento;
        this.piso = piso;
    }

    public void setPiso(String piso){
        this.piso = piso;
    }
    public String getPiso(){
        return piso;
    }

    public void setDepartamento(String departamento){
        this.departamento = departamento;
    }
    public String getDepartamento(){
        return departamento;
    }

    @Override
    public String mostrarDatosDeDomicilio(){
        return "Calle: "+getCalle() + "Numero: "+getnumeroDeCasa() + "Piso: "+piso + "Departamento: "+departamento;
    }
}