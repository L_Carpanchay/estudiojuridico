package estudiojuridico.estudio;

public class DomicilioBarrial extends Domicilio{
    private String manzana;
    private String barrio;

    public DomicilioBarrial(String calle, int numeroDeCasa, String manzana, String barrio){
        super(calle, numeroDeCasa);
        this.manzana = manzana;
        this.barrio = barrio;
    }

    public void setManzana(String manzana){
        this.manzana = manzana;
    }
    public String getManzana(){
        return manzana;
    }

    public void setBarrio(String barrio){
        this.barrio = barrio;
    }
    public String getBarrio(){
        return barrio;
    }

    @Override
    public String mostrarDatosDeDomicilio(){
        return "Calle: "+getCalle() + "Numero: "+getnumeroDeCasa()+"Manazna: "+manzana+ "Barrio: "+barrio;
    }
}