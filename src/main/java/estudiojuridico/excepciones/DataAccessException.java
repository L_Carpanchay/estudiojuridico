package estudiojuridico.excepciones;

public class DataAccessException extends RuntimeException {
    public DataAccessException() {
        super("Completar todos los campos");
    }
}
