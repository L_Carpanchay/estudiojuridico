package estudiojuridico.excepciones;

public class ClienteInexistenteException extends RuntimeException{
    public ClienteInexistenteException(){
        super("El cliente no existe");
    }
}
